package pl.mszarlinski.gitlab.calculator

import spock.lang.Subject
import spock.lang.Unroll

/**
 * @author mszarlinski
 */
class CalculatorTest extends spock.lang.Specification {

    @Subject
    final Calculator tested = new Calculator()

    @Unroll
    def "Should sum #x and #y and return #expected"() {
        when:
            int sum = tested.sum(x, y)
        then:
            sum == expected
        where:
            x  | y  || expected
            2  | 2  || 4
            5  | 0  || 5
            2  | -1 || 1
            -3 | -2 || -5
    }

    @Unroll
    def "Should divide #x by #y and return #expected"() {
        when:
            double quotient = tested.div(x, y)
        then:
            quotient == expected
        where:
            x   | y  || expected
            2   | 2  || 1
            8   | -4 || -2
            0   | 1  || 0
            -3  | 2  || -1.5
            -10 | -4 || 2.5
            5   | 0  || 0
    }
}
