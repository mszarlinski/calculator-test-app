package pl.mszarlinski.gitlab.calculator;

/**
 * @author mszarlinski
 */
public class Calculator {

    public int sum(int x, int y) {
        return x + y;
    }

    public double div(int x, int y) {
        if (y == 0) {
            return 0;
        }
        return (double) x / y;
    }

    public int mul(int x, int y) {
        return x * y;
    }
}
